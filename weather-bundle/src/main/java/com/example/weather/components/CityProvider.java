package com.example.weather.components;

import com.example.weather.components.gen.CityProviderDefinition;
import com.example.weather.content.gen.CityProviderContent;

import de.semvox.odp.ltm.DefaultStaticSlotContentProvider;

public class CityProvider extends DefaultStaticSlotContentProvider {

	public CityProvider() throws Exception {
		super(new CityProviderDefinition().loadThing(),
				new CityProviderContent().loadThing().getCommonContentList());
	}

}
