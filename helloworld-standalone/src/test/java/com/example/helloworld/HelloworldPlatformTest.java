package com.example.helloworld;

import de.semvox.odp.s3.test.junit.PlatformTest;
import de.semvox.commons.io.locator.ResourceLocator.LocatorType;
import com.example.helloworld.specification.gen.HelloworldSpecification;
import de.semvox.types.odp.specification.Specification;

public class HelloworldPlatformTest extends PlatformTest {

	@Override
    protected LocatorType getRtDefLocatorType() {
        return LocatorType.file;
    }

    @Override
    protected String getRtDefLocation() {
        return "data/rt_test.xml";
    }

    @Override
    protected Specification getSpecification() {
        return new HelloworldSpecification().loadThing();
    }
}
