taskmodel {
  tasks {
    executable-task GET_WEATHER {
      description "A task to get the weather report."
      task-confirmation-directives { 
        always-confirm-execution {
          recognition-quality-constraint none
        }
      }
      cancelation-directive none
      interruption-directive cancel-on-interruption
      execution direct-execution
      use-hypothesis-fusion yes
      pattern {
        object weather#GetWeather {
          property loc#city slot: CITY;
        }
      }
      slots {
        slot CITY by-user ( functional , City ) {
          type mandatory
          correction not-correctable
          ellipsis-directive allow-ellipsis-on-task-termination {
              grammar-activation {
                  recognition-mode inherited
                  inherit yes
                  includes {
                    object nlu#PreloadedGrammarSelector {
                        property common#name {
                          "ContextEllipsisGrammar"
                        }
                    }
                  }
                  excludes {
                  }
              }
          }
          pattern {
            object loc#City ;
          }
        }
      }
    }
  }
}