package com.example.weather.components;

import java.util.Map;

import com.example.ontologies.weather.GetWeather;
import com.example.ontologies.weather.WeatherReport;
import com.example.ontologies.weather.factory.ThingFactoryWeather;
import com.example.weather.components.gen.WeatherServiceDefinition;

import de.semvox.commons.Randomizer;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class WeatherService implements Service {

	private final ServiceDefinition definition;

	public WeatherService() throws Exception {
		definition = new WeatherServiceDefinition().loadThing();
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		if (request instanceof GetWeather) {
			WeatherReport report = ThingFactoryWeather.newWeatherReport();
			if (Randomizer.tossTheCoin()) {
				report.setWeatherWeatherState(ThingFactoryWeather.newRainy());
			} else {
				report.setWeatherWeatherState(ThingFactoryWeather.newSunny());
			}
			((GetWeather) request).setWeatherWeatherReport(report);
			request.setIntResponseStatus(ThingFactoryIntegration
					.newStatusSuccess());
		}
		return request;
	}

}
