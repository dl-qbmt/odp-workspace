package com.example.weather;

import java.util.ArrayList;
import java.util.List;

import de.semvox.odp.multimodal.LanguageProvider;
import de.semvox.odp.s3.Platform;
import de.semvox.odp.s3.bundle.AbstractDialogBundle;
import de.semvox.types.odp.spijk.StandaloneGrammar;
import de.semvox.types.odp.spit.TemplateCollection;
import de.semvox.types.odp.squint.TaskModel;

import com.example.weather.components.CityProvider;
import com.example.weather.components.WeatherService;
import com.example.weather.nlu.gen.ContextEllipsisGrammar;
import com.example.weather.nlu.gen.WeatherGrammar;
import com.example.weather.nlg.gen.WeatherTemplates;
import com.example.weather.taskmodel.gen.WeatherTaskmodel;

public class WeatherBundle extends AbstractDialogBundle {

	@Override
	protected void onLoad(Platform platform) throws Exception {
		// initialize bundle resources

		// initialize grammar
		List<StandaloneGrammar> grammars = getGrammars(platform);

		// initialize template collection
		List<TemplateCollection> templates = getTemplates(platform);

		// initialize taskmodel
		TaskModel taskModel = new WeatherTaskmodel().loadThing();

		loadBundleResources(grammars, taskModel, templates, platform);

		// TODO implement onLoad
		platform.registerComponent(new WeatherService());
		CityProvider cityProvider = new CityProvider();
		platform.getLongTermMemory().registerSlotContentProvider(cityProvider,
				cityProvider.getDefinition());

	}

	protected List<StandaloneGrammar> getGrammars(Platform platform) {
		LanguageProvider langProvider = platform.getLanguageProvider();

		List<StandaloneGrammar> grammars = new ArrayList<StandaloneGrammar>();
		grammars.add(new WeatherGrammar(langProvider).loadThing());

		// adding the ContextEllipsisGrammar
		grammars.add(new ContextEllipsisGrammar(langProvider).loadThing());

		return grammars;
	}

	protected List<TemplateCollection> getTemplates(Platform platform) {
		LanguageProvider langProvider = platform.getLanguageProvider();

		List<TemplateCollection> templates = new ArrayList<TemplateCollection>();
		templates.add(new WeatherTemplates(langProvider).loadThing());

		return templates;
	}

	@Override
	public void changeLanguage(String language, Platform platform)
			throws Exception {
		reloadGrammars(getGrammars(platform), platform);
		reloadTemplates(getTemplates(platform), platform);
	}

	@Override
	protected void onUnload(Platform arg0) {
		// TODO implement onUnLoad

	}
}